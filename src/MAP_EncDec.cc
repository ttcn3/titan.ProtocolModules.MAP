//  Reference:          3GPP TS 29.002 10.6.0

#include "MAP_PDU_Defs.hh"
#include "MAP_DialogueInformation.hh"

namespace MAP__Types {

using namespace MAP__DialogueInformation;

TTCN_Module MAP__EncDec("MAP_EncDec", __DATE__, __TIME__);

//////////////////////////////////
// Encoding function for TCAP_MAP_user_information
//////////////////////////////////

OCTETSTRING enc__TCAP__MAP__user__information
(const TCAP__MAP__user__information& pdu)
{
  Seq__of__ANY__MAP tmp_seqofany=NULL_VALUE;
  TTCN_Buffer buf;
  for(int i=0; i<pdu.size_of(); i++) 
  {
     MAP__DialoguePDU__inEXTERNAL tmp_ext;
     tmp_ext.direct__reference()=pdu[i].syntax();
     tmp_ext.encoding().single__ASN1__type()
        =pdu[i].mAP__DialoguePDU();
     buf.clear();
 
  tmp_ext.encode(MAP__DialoguePDU__inEXTERNAL_descr_, buf,
		 TTCN_EncDec::CT_BER, BER_ENCODE_DER);
  tmp_seqofany[i]=OCTETSTRING(buf.get_len(), buf.get_data());		 
 }	 
  buf.clear();
  tmp_seqofany.encode(Seq__of__ANY__MAP_descr_, buf,
                      TTCN_EncDec::CT_BER, BER_ENCODE_DER);
  return OCTETSTRING(buf.get_len(), buf.get_data());
}



//////////////////////////////////
// Decoding function for TCAP_MAP_user_information
//////////////////////////////////

TCAP__MAP__user__information dec__TCAP__MAP__user__information
(const OCTETSTRING& stream)
{
  TTCN_Buffer buf;
  Seq__of__ANY__MAP tmp_seqofany;
  buf.put_os(stream);
  tmp_seqofany.decode(Seq__of__ANY__MAP_descr_, buf,
                      TTCN_EncDec::CT_BER, BER_ACCEPT_ALL);
  TCAP__MAP__user__information pdu=NULL_VALUE;
  for(int i=0; i<tmp_seqofany.size_of(); i++) 
    {
    buf.clear();
    buf.put_os(tmp_seqofany[i]);
    MAP__DialoguePDU__inEXTERNAL tmp_ext;
    tmp_ext.decode(MAP__DialoguePDU__inEXTERNAL_descr_, buf,
		 TTCN_EncDec::CT_BER, BER_ACCEPT_ALL);
 
    pdu[i].syntax()=tmp_ext.direct__reference();
    pdu[i].mAP__DialoguePDU()=tmp_ext.encoding().single__ASN1__type();
   } 
   return pdu;
}


//////////////////////////////////
// Decoding function for MAP__DialoguePDU
//////////////////////////////////
MAP__DialoguePDU dec__MAP__DialoguePDU(const OCTETSTRING& stream)
{
  TTCN_Buffer buf;
  MAP__DialoguePDU pdu;
  buf.put_os(stream);
  pdu.decode(MAP__DialoguePDU_descr_, buf, TTCN_EncDec::CT_BER, BER_ACCEPT_ALL);
  return pdu;
}

//////////////////////////////////
// Encoding function for MAP__Invoke
// Returns only argument field
//////////////////////////////////

OCTETSTRING enc__MAP__Invoke(const MAP__PDU__Defs::MAP__Invoke& pdu) {
	TTCN_Buffer buf;

	pdu.encode(MAP__PDU__Defs::MAP__Invoke_descr_, buf, TTCN_EncDec::CT_BER, BER_ENCODE_DER);
	MAP__PDU__Defs::MAP__Invoke__help help_pdu;
	help_pdu.decode(MAP__PDU__Defs::MAP__Invoke__help_descr_, buf, TTCN_EncDec::CT_BER, BER_ACCEPT_ALL);
	return (OCTETSTRING&) help_pdu.argument();
}

//////////////////////////////////
// Decoding function for MAP__Invoke
//////////////////////////////////

MAP__PDU__Defs::MAP__Invoke dec__MAP__Invoke(const INTEGER& invokeId, 
	const INTEGER& linkedId, const Remote__Operations__Information__Objects::Code& opcode, const OCTETSTRING& stream) {
	
	MAP__PDU__Defs::MAP__Invoke__help help_pdu;
	MAP__PDU__Defs::MAP__Invoke pdu;
	
	help_pdu.invokeId().present__() = invokeId;
	help_pdu.linkedId()().present__().present__() = linkedId;
	help_pdu.opcode() = opcode;
	help_pdu.argument() = stream;
	
	TTCN_Buffer buf;

	help_pdu.encode(MAP__PDU__Defs::MAP__Invoke__help_descr_, buf, TTCN_EncDec::CT_BER, BER_ENCODE_DER);	
	pdu.decode(MAP__PDU__Defs::MAP__Invoke_descr_, buf, TTCN_EncDec::CT_BER, BER_ACCEPT_ALL);

	return pdu;
}

//////////////////////////////////
// Encoding function for MAP__ReturnResult
//////////////////////////////////

OCTETSTRING enc__MAP__ReturnResult(const MAP__PDU__Defs::MAP__ReturnResult& pdu) {
	TTCN_Buffer buf;

	pdu.encode(MAP__PDU__Defs::MAP__ReturnResult_descr_, buf, TTCN_EncDec::CT_BER, BER_ENCODE_DER);	
	
	MAP__PDU__Defs::MAP__ReturnResult__help help_pdu;
	
	help_pdu.decode(MAP__PDU__Defs::MAP__ReturnResult__help_descr_, buf, TTCN_EncDec::CT_BER, BER_ACCEPT_ALL);
	return (OCTETSTRING&) help_pdu.result()().result();
}

//////////////////////////////////
// Decoding function for MAP__ReturnResult
//////////////////////////////////

MAP__PDU__Defs::MAP__ReturnResult dec__MAP__ReturnResult(const INTEGER& invokeId, 
	const Remote__Operations__Information__Objects::Code& opcode, const OCTETSTRING& stream) {
	
	MAP__PDU__Defs::MAP__ReturnResult__help help_pdu;
	MAP__PDU__Defs::MAP__ReturnResult pdu;
	
	help_pdu.invokeId().present__() = invokeId;
	help_pdu.result()().opcode() = opcode;
	help_pdu.result()().result() = stream;
	
	TTCN_Buffer buf;

	help_pdu.encode(MAP__PDU__Defs::MAP__ReturnResult__help_descr_, buf, TTCN_EncDec::CT_BER, BER_ENCODE_DER);	

	pdu.decode(MAP__PDU__Defs::MAP__ReturnResult_descr_, buf, TTCN_EncDec::CT_BER, BER_ACCEPT_ALL);
	return pdu;
}

//////////////////////////////////
// Encoding function for MAP__ReturnError
//////////////////////////////////

OCTETSTRING enc__MAP__ReturnError(const MAP__PDU__Defs::MAP__ReturnError& pdu) {
	TTCN_Buffer buf;

	pdu.encode(MAP__PDU__Defs::MAP__ReturnError_descr_, buf, TTCN_EncDec::CT_BER, BER_ENCODE_DER);	
	MAP__PDU__Defs::MAP__ReturnError__help help_pdu;
	
	help_pdu.decode(MAP__PDU__Defs::MAP__ReturnError__help_descr_, buf, TTCN_EncDec::CT_BER, BER_ACCEPT_ALL);
	return (OCTETSTRING&) help_pdu.parameter()();
}

//////////////////////////////////
// Decoding function for MAP__ReturnError
//////////////////////////////////

MAP__PDU__Defs::MAP__ReturnError dec__MAP__ReturnError(const INTEGER& invokeId, 
	const Remote__Operations__Information__Objects::Code& errorCode, const OCTETSTRING& stream) {
	
	MAP__PDU__Defs::MAP__ReturnError__help help_pdu;
	MAP__PDU__Defs::MAP__ReturnError pdu;
	
	help_pdu.invokeId().present__() = invokeId;
	help_pdu.errcode() = errorCode;
	help_pdu.parameter()() = stream;
	
	TTCN_Buffer buf;

	help_pdu.encode(MAP__PDU__Defs::MAP__ReturnError__help_descr_, buf, TTCN_EncDec::CT_BER, BER_ENCODE_DER);	
	pdu.decode(MAP__PDU__Defs::MAP__ReturnError_descr_, buf, TTCN_EncDec::CT_BER, BER_ACCEPT_ALL);

	return pdu;
}

}//namespace
